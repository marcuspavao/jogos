import adivinhacao
import forca

def escolhe_jogo():
    print("Escolha o jogo")
    print("Para jogar o jogo da Adivinhação digite 2 e para o jogo da Forca digite 1")

    jogo = int(input("Qual jogo?: "))

    if(jogo == 1):
        forca.jogar()
    elif(jogo == 2):
        adivinhacao.jogar()

if(__name__ == "__main__"):
    escolhe_jogo()