import random
def jogar():
    print("Bem vindo ao jogo de adivinhação")

    numero_secreto = random.randrange(1,101)
    """ print(numero_secreto) """

    print("Qual o nível de dificuldade?\n","(1) Fácil (2) Médio (3) Difícil")
    nivel = int(input("Escolha: "))

    facil = nivel == 1
    medio = nivel == 2
    dificil = nivel == 3

    if(facil):
        numero_de_tentativas = 20
    elif(medio):
        numero_de_tentativas = 10
    elif(dificil):
        numero_de_tentativas = 5
    else:
        print("Você não digitou um número válido")
        numero_de_tentativas = 0

    pontos = 1000

    for rodada in range(1, numero_de_tentativas + 1):

        print(f"Tentativa: {rodada} de {numero_de_tentativas}")

        chute = int(input("Digite seu número entre 1 e 100: "))
        print("Você digitou ", chute)

        if (chute < 1 or chute > 100):
            print("Você deve digitar um número entre 1 e 100!")
            continue

        acertou = numero_secreto == chute
        maior = numero_secreto < chute
        menor = numero_secreto > chute

        if(acertou):
            print(f"Acertou e fez {pontos}")
            break
        else:
            if(maior):
                print("Errou, chute um número menor")
                if(rodada == numero_de_tentativas):
                    print(f"O número secreto era {numero_secreto} e você fez {pontos}")
            if(menor):
                print("Errou, chute um número maior")
                if(rodada == numero_de_tentativas):
                    print(f"O número secreto era {numero_secreto} e você fez {pontos}")
            pontos_perdidos = abs(numero_secreto - chute)
            pontos = pontos - pontos_perdidos

    print("Fim do jogo")

if(__name__ == "__main__"):
    jogar()

    """ while(numero_de_tentativas >= rodada):

        print("Tentativa: {} de {}".format(rodada, numero_de_tentativas))

        chute = int(input("Digite seu número "))
        print("Você digitou ", chute)

        acertou = numero_secreto == chute
        maior = numero_secreto < chute
        menor = numero_secreto > chute

        if(acertou):
            print("Acertou")
            break
        else:
            if(maior):
                print("Errou, chute um número menor")
            if(menor):
                print("Errou, chute um número maior")
        
        rodada = rodada + 1 """